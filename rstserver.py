import json
import os
import signal
import sys

from docutils.core import publish_string


OVERRIDES = {'embed_stylesheet': False,
             'smart_quotes': True,
             'syntax_highlight': 'short',
             'trim_footnote_reference_space': True,
             'initial_header_level': 2}
LOGFILE = 'rstserver.log'


def signal_handler(_sig, _frame):
    sys.exit(0)


def assoc(d, **kvs):
    return({**d, **kvs})


def flog(string):
    if os.getenv('DEBUG'):
        with open(LOGFILE, 'a') as f:
            f.write(string + '\n')


def main():
    command = ''
    while command != 'quit':
        line = sys.stdin.readline()
        flog(f'Read line {repr(line)}')
        data = json.loads(line)
        flog(f'Read data {repr(data)}')
        command = data['command']
        if command == 'translate':
            overrides = {}
            if 'slug' in data and data['slug']:
                id_prefix = data['slug'] + '_%'
                overrides = assoc(OVERRIDES, auto_id_prefix=id_prefix)

            output = publish_string(data['string'],
                                    writer_name='html',
                                    settings_overrides=overrides)
            line = json.dumps({'output': output.decode()})
            flog(f'Writing line {repr(line)}')
            sys.stdout.write(line)
            sys.stdout.write('\n')
            sys.stdout.flush()


if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    main()
